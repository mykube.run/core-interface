package dependency

import (
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"gitlab.com/mykube.run/core-interface/config"
	"gitlab.com/mykube.run/core-interface/configmanager"
	"go.uber.org/zap"
)

type Dependency struct {
	cm     configmanager.Manager
	errors []error
	lg     *zap.Logger

	db        *gorm.DB
	redis     *redis.Client
	aliyunOSS *oss.Client

	Configs Configs
}

type Configs struct {
	Service   *config.ServiceConfig
	Log       *config.LogConfig       `json:"log" yaml:"log"`
	Mysql     *config.MysqlConfig     `json:"mysql" yaml:"mysql"`
	Redis     *config.RedisConfig     `json:"redis" yaml:"redis"`
	AliyunOSS *config.AliyunOSSConfig `json:"aliyun_oss" yaml:"aliyun_oss"`
}

func NewDefaultDependency(cm configmanager.Manager) *Dependency {
	s := &Dependency{}
	s.cm = cm
	s.errors = make([]error, 0)
	s.lg, _ = zap.NewProduction()
	s.Configs = Configs{}
	return s
}

func (s *Dependency) CustomizedLogEnabled() bool {
	return s.Configs.Log != nil
}

func (s *Dependency) ServiceEnabled() bool {
	return s.Configs.Service != nil
}

func (s *Dependency) MysqlEnabled() bool {
	return s.Configs.Mysql != nil
}

func (s *Dependency) RedisEnabled() bool {
	return s.Configs.Redis != nil
}

func (s *Dependency) AliyunOSSEnabled() bool {
	return s.aliyunOSS != nil
}

func (s *Dependency) WithCustomizedLogger() *Dependency {
	s.Configs.Log = new(config.LogConfig)

	var fn = func() error {
		l, ce := s.Configs.Log.SetupLogger()
		if ce != nil {
			return ce
		}
		s.lg = l
		return nil
	}

	err := s.cm.RegisterWithCallback("log", s.Configs.Log, fn)

	if err != nil {
		s.errors = append(s.errors, err)
	}
	return s
}

func (s *Dependency) WithService() *Dependency {
	s.Configs.Service = new(config.ServiceConfig)

	err := s.cm.Register("service", s.Configs.Service)
	if err != nil {
		s.errors = append(s.errors, err)
	}
	return s
}

func (s *Dependency) WithMysql() *Dependency {
	s.Configs.Mysql = new(config.MysqlConfig)

	var fn = func() error {
		db, ce := s.Configs.Mysql.Connect()
		if ce != nil {
			return ce
		}

		s.db = db
		s.lg.Info("connected to mysql", zap.String("address", s.Configs.Mysql.Anonymized()))
		return nil
	}
	err := s.cm.RegisterWithCallback("mysql", s.Configs.Mysql, fn)
	if err != nil {
		s.errors = append(s.errors, err)
	}
	return s
}

func (s *Dependency) WithRedis() *Dependency {
	s.Configs.Redis = new(config.RedisConfig)

	var fn = func() error {
		r, ce := s.Configs.Redis.Connect()
		if ce != nil {
			return ce
		}
		s.redis = r
		s.lg.Info("connected to redis", zap.String("address", s.Configs.Redis.Anonymized()))
		return nil
	}

	err := s.cm.RegisterWithCallback("redis", s.Configs.Redis, fn)
	if err != nil {
		s.errors = append(s.errors, err)
	}
	return s
}

func (s *Dependency) WithAliyunOSS() *Dependency {
	s.Configs.AliyunOSS = new(config.AliyunOSSConfig)
	var fn = func() error {
		oc, ce := s.Configs.AliyunOSS.Connect()
		if ce != nil {
			return ce
		}
		s.aliyunOSS = oc
		s.lg.Info("connected to oss", zap.String("address", s.Configs.AliyunOSS.Anonymized()))
		return nil
	}
	err := s.cm.RegisterWithCallback("aliyun_oss", s.Configs.AliyunOSS, fn)
	if err != nil {
		s.errors = append(s.errors, err)
	}
	return s
}

func (s *Dependency) Logger() *zap.Logger {
	return s.lg
}

func (s *Dependency) Mysql() *gorm.DB {
	return s.db
}

func (s *Dependency) Redis() *redis.Client {
	return s.redis
}

func (s *Dependency) AliyunOSS() *oss.Client {
	return s.aliyunOSS
}

func (s *Dependency) Errors() []error {
	return s.errors
}
