package dependency

import (
	"gitlab.com/mykube.run/core-interface/configmanager"
	"os"
	"testing"
)

func Test_Errors(t *testing.T) {
	ns := "/micro/config/config-manager-test"
	// setup bootstrap envs
	bs := configmanager.DefaultBootstrapEnvs()
	if err := os.Setenv(bs.SourceType, configmanager.SourceTypeConsul); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv(bs.ServerAddr, "0.0.0.0:8500"); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv(bs.Group, "test"); err != nil {
		t.Fatal(err)
	}

	cm, err := configmanager.NewDefaultConfigManager(bs, ns)
	if err != nil {
		t.Fatal(err)
	}
	dep := NewDefaultDependency(cm)

	errs := dep.WithAliyunOSS().Errors()
	if len(errs) != 0 {
		t.Fatal(errs)
	}
}
