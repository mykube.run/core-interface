package model

type BaseModel struct {
	Id        uint   `gorm:"primary_key" json:"id" msgpack:"id"`
	CreatedAt int64  `gorm:"column:created_at" json:"created_at" msgpack:"created_at"`
	UpdatedAt int64  `gorm:"column:updated_at" json:"updated_at" msgpack:"updated_at"`
	DeletedAt *int64 `gorm:"index,null,column:deleted_at" json:"deleted_at" msgpack:"deleted_at"`
}

type BaseVo struct {
	Id        uint  `json:"id"`
	CreatedAt int64 `json:"created_at"`
	UpdatedAt int64 `json:"updated_at"`
}

type DeleteResult struct {
	Deleted interface{} `json:"deleted"`
}
