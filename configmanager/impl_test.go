package configmanager

import (
	"fmt"
	"gitlab.com/mykube.run/core-interface/config"
	"os"
	"testing"
	"time"
)

func Test_DefaultConfigManager(t *testing.T) {
	// setup bootstrap envs
	bs := DefaultBootstrapEnvs()
	if err := os.Setenv(bs.SourceType, SourceTypeConsul); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv(bs.ServerAddr, "0.0.0.0:8500"); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv(bs.Group, "test"); err != nil {
		t.Fatal(err)
	}

	// create config manager
	cm, err := NewDefaultConfigManager(bs, "/micro/config/config-manager-test")
	if err != nil {
		t.Fatal(err)
	}

	// register SampleConfig from sample
	var c config.SampleConfig
	var fn = func() error {
		fmt.Println("config changed", string(cm.config.Bytes()), c)
		return nil
	}

	if err := cm.RegisterWithCallback("sample", &c, fn); err != nil {
		t.Fatal(err)
	}

	if c.StringVal != "some string" {
		t.Fatalf("string val incorrect")
	}
	fmt.Println(c)

	// test watch
	fmt.Println("waiting for changes happen")
	time.Sleep(time.Second * 10)

	fmt.Println(c)
	fmt.Println(cm.Close())

	//// register another SampleConfig from sample2
	//var c2 config.AliyunOSSConfig
	//if err := cm.Register("sample2", &c2); err != nil {
	//	t.Fatal(err)
	//}
	//
	//if err := cm.Close(); err != nil {
	//	t.Fatal(err)
	//}
}
