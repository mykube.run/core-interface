package configmanager

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	microconfig "github.com/micro/go-micro/config"
	"github.com/micro/go-micro/config/encoder/yaml"
	"github.com/micro/go-micro/config/source"
	"github.com/micro/go-micro/config/source/etcd"
	"github.com/micro/go-micro/v2/config/reader"
	"github.com/micro/go-plugins/config/source/consul"
	"gitlab.com/mykube.run/core-interface/config"
	"os"
	"time"
)

const (
	SourceTypeConsul = "consul"
	SourceTypeEtcd   = "etcd"
)

const (
	DefaultBootstrapEnvSourceType = "CONFIG_SOURCE_TYPE"
	DefaultBootstrapEnvServerAddr = "CONFIG_SERVER_ADDR"
	DefaultBootstrapEnvGroup      = "CONFIG_GROUP"
)

type BootstrapEnvs struct {
	SourceType string
	ServerAddr string
	Group      string
}

func DefaultBootstrapEnvs() BootstrapEnvs {
	return BootstrapEnvs{
		SourceType: DefaultBootstrapEnvSourceType,
		ServerAddr: DefaultBootstrapEnvServerAddr,
		Group:      DefaultBootstrapEnvGroup,
	}
}

type watched struct {
	name string
	c    config.Config
	fn   func() error
}

type DefaultConfigManager struct {
	namespace string
	group     string

	bootstrap BootstrapEnvs

	config microconfig.Config

	closed   bool
	watcher  microconfig.Watcher
	handlers []watched
}

func (m *DefaultConfigManager) Register(name string, val config.Config) error {
	var noop = func() error {
		return nil
	}
	err := m.RegisterWithCallback(name, val, noop)
	return err
}

func (m *DefaultConfigManager) RegisterWithCallback(name string, val config.Config, fn func() error) error {
	err := m.scan(name, m.config.Get(m.group), val)
	if err != nil {
		return err
	}
	err = val.Validate()
	if err != nil {
		return fmt.Errorf("validate config error: %v, current value: %s", err, m.config.Get(name).Bytes())
	}

	err = fn()
	if err != nil {
		return fmt.Errorf("error executing callback while registering config %s: %v", name, err)
	}

	m.handlers = append(m.handlers, watched{
		name: name,
		c:    val,
		fn:   fn,
	})
	return nil
}

func (m *DefaultConfigManager) scan(name string, in reader.Value, out config.Config) error {
	var sv = make(map[string]interface{})
	if err := json.Unmarshal(in.Bytes(), &sv); err != nil {
		return fmt.Errorf("error unmarshaling group %v to json: %v", m.group, err)
	}

	if _, ok := sv[name]; !ok {
		return fmt.Errorf("error registering config %s: no entry found", name)
	}

	byt, err := json.Marshal(sv[name])
	if err != nil {
		return err
	}
	if err := out.Unmarshal(config.EncJson, byt); err != nil {
		return fmt.Errorf("error unmarshaling config with json encoding: %v", err)
	}
	return nil
}

func (m *DefaultConfigManager) Close() error {
	m.closed = true

	if err := m.config.Close(); err != nil {
		return err
	}
	if err := m.watcher.Stop(); err != nil {
		return err
	}

	return nil
}

func (m *DefaultConfigManager) Get(name string) (val []byte, err error) {
	var sv = make(map[string]interface{})
	if err = json.Unmarshal(m.config.Bytes(), &sv); err != nil {
		return nil, fmt.Errorf("error unmarshaling group %v to json: %v", m.group, err)
	}

	if _, ok := sv[name]; !ok {
		return nil, fmt.Errorf("error registering config %s: no entry found", name)
	}

	val, err = json.Marshal(sv[name])
	return
}

func NewDefaultConfigManager(bootstrap BootstrapEnvs, namespace string) (m *DefaultConfigManager, err error) {
	addr := os.Getenv(bootstrap.ServerAddr)
	srcType := os.Getenv(bootstrap.SourceType)
	group := os.Getenv(bootstrap.Group)
	if addr == "" || (srcType != SourceTypeConsul && srcType != SourceTypeEtcd) || group == "" {
		return nil, fmt.Errorf("invalid bootstrap envs: %+v", bootstrap)
	}

	var src source.Source
	prefix := fmt.Sprintf("%s", namespace)
	e := yaml.NewEncoder()
	if srcType == SourceTypeEtcd {
		src = etcd.NewSource(etcd.WithPrefix(prefix), etcd.WithAddress(addr), source.WithEncoder(e), etcd.StripPrefix(true))
	} else {
		src = consul.NewSource(consul.WithPrefix(prefix), consul.WithAddress(addr), source.WithEncoder(e), consul.StripPrefix(true))
	}

	conf := microconfig.NewConfig()
	if err = conf.Load(src); err != nil {
		fmt.Println("hint: try to replace tabs with spaces in yaml")
		return nil, err
	}
	fmt.Printf("loaded config from source: %v\n", prefix)

	m = &DefaultConfigManager{
		namespace: namespace,
		group:     group,
		bootstrap: bootstrap,
		config:    conf,
	}

	w, err := m.config.Watch(m.group)
	if err != nil {
		return nil, err
	}
	m.watcher = w
	m.handlers = make([]watched, 0)

	go func() {
		for {
			if m.closed {
				fmt.Println("config manager closed, stop watching")
				break
			}

			// 1. Watch for changes
			v, err := m.watcher.Next()
			if err != nil {
				fmt.Printf("error watching config %s: %v\n", group, err)
				time.Sleep(time.Second)
				continue
			}

			for _, h := range m.handlers {
				// 2. Scan into copied val to check hash
				cp := h.c.Copy()
				if err := m.scan(h.name, v, cp); err != nil {
					fmt.Printf("error scanning updated config %s: %v\n", h.name, err)
					time.Sleep(time.Second)
					continue
				}

				// 3. Validate updated config
				if err := cp.Validate(); err != nil {
					fmt.Printf("updated config validate error: %v\n", err)
					time.Sleep(time.Second)
					continue
				}

				// 4. Check whether value has been changed. If not, enter next loop.
				if !h.c.Updated(cp.Hash()) {
					fmt.Printf("config %s did not change since last watch\n", h.name)
					time.Sleep(time.Second)
					continue
				} else {
					hx := hex.EncodeToString(cp.Hash())
					fmt.Printf("config %s will update to: %s, hash: %s\n", h.name, cp.Anonymized(), hx)
				}

				// 5. Scan into val
				_ = m.scan(h.name, v, h.c)
				if err := h.c.Validate(); err != nil {
					fmt.Printf("config validate error: %v\n", err)
					time.Sleep(time.Second)
					continue
				}

				// 6. Call callback fn
				if err := h.fn(); err != nil {
					fmt.Printf("error executing callback for config %s: %v\n", h.name, err)
				}
			}
		}
	}()

	return m, nil
}
