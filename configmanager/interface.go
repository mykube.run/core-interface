package configmanager

import "gitlab.com/mykube.run/core-interface/config"

type Manager interface {
	Register(name string, val config.Config) error
	RegisterWithCallback(name string, val config.Config, fn func() error) error
	Close() error
	Get(name string) ([]byte, error)
}
