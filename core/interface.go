package core

import (
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"gitlab.com/mykube.run/core-interface/configmanager"
	"gitlab.com/mykube.run/core-interface/dependency"
	"go.uber.org/zap"
)

type DBConn interface {
	DB() *gorm.DB
	DBRead() *gorm.DB
	Logger() *zap.Logger
	CtxLogger(ctx *gin.Context) *zap.Logger
}

type CacheDBConn interface {
	DB() *gorm.DB
	DBRead() *gorm.DB
	Redis() *redis.Client
	Logger() *zap.Logger
	CtxLogger(ctx *gin.Context) *zap.Logger
}

type Engine interface {
	Engine() *gin.Engine
	Wrapper(fn func(ctx *gin.Context) error) gin.HandlerFunc
	Run() error
}

type Interface interface {
	// DB Connection. Default to write DB
	DB() *gorm.DB

	// Explicit Transaction. Default to write DB
	Tx() DBConn

	// DB Connection for read
	DBRead() *gorm.DB

	// Cache & DB/Tx Connection
	CacheDB(tx bool) CacheDBConn

	// Redis Connection
	Redis() *redis.Client

	// Zap Logger
	Logger() *zap.Logger

	// Zap Logger with context
	CtxLogger(ctx *gin.Context) *zap.Logger

	// Dependencies
	Dep() *dependency.Dependency

	// Config Manager
	ConfigManager() configmanager.Manager

	// Simple KV
	KVSet(k string, val interface{})
	KVGet(k string) (interface{}, bool)

	// gin Engine
	Engine() *gin.Engine

	// Custom gin handle func Wrapper
	Wrapper(fn func(ctx *gin.Context) error) gin.HandlerFunc

	// Run gin engine
	Run() error
}
