package core

import (
	"gitlab.com/mykube.run/core-interface/configmanager"
	"gitlab.com/mykube.run/core-interface/dependency"
)

// The singleton
var ins *impl

func I() *impl {
	return ins
}

func New(m configmanager.Manager, dep *dependency.Dependency) (*impl, error) {
	var kv = make(map[string]interface{})
	ins = &impl{
		kv:      kv,
		dep:     dep,
		manager: m,
	}
	return ins, nil
}
