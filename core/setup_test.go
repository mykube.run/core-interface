package core

import (
	"gitlab.com/mykube.run/core-interface/configmanager"
	"os"
	"testing"
)

func Test_New(t *testing.T) {
	var err error
	// setup bootstrap envs
	bs := configmanager.DefaultBootstrapEnvs()
	if err = os.Setenv(bs.SourceType, configmanager.SourceTypeConsul); err != nil {
		t.Fatal(err)
	}
	if err = os.Setenv(bs.ServerAddr, "0.0.0.0:8500"); err != nil {
		t.Fatal(err)
	}
	if err = os.Setenv(bs.Group, "testing"); err != nil {
		t.Fatal(err)
	}

	manager, err := configmanager.NewDefaultConfigManager(bs, "/micro/config/core-interface")
	if err != nil {
		t.Fatal(err)
	}
	ins, err := New(manager, NewDefaultDependencies())
	if err != nil {
		t.Fatal(err)
	}
	if ins == nil {
		t.Fatal("instance is nil")
	}

	if Instance() == nil {
		t.Fatal("instance() is nil")
	}
}
