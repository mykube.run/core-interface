package core

import (
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/mykube.run/core-interface/configmanager"
	"gitlab.com/mykube.run/core-interface/dependency"
	"gitlab.com/mykube.run/core-interface/engine"
	"go.uber.org/zap"
	"sync"
)

// DB connection, Can be used in DAOs.
type conn struct {
	db     *gorm.DB
	dbRead *gorm.DB
	logger *zap.Logger
}

func (c *conn) DB() *gorm.DB {
	return c.db
}

func (c *conn) DBRead() *gorm.DB {
	if c.dbRead != nil {
		return c.dbRead
	}
	return c.db
}

func (c *conn) Logger() *zap.Logger {
	return c.logger
}

func (c *conn) CtxLogger(ctx *gin.Context) *zap.Logger {
	l := c.logger.With(zap.String("request_id", engine.GetRequestId(ctx)))
	return l
}

// DB connection with redis as cache, Can be used in DAOs.
type cacheDB struct {
	db     *gorm.DB
	dbRead *gorm.DB
	redis  *redis.Client
	logger *zap.Logger
}

func (c *cacheDB) DB() *gorm.DB {
	return c.db
}

func (c *cacheDB) DBRead() *gorm.DB {
	if c.dbRead != nil {
		return c.dbRead
	}
	return c.db
}

func (c *cacheDB) Redis() *redis.Client {
	return c.redis
}

func (c *cacheDB) Logger() *zap.Logger {
	return c.logger
}

func (c *cacheDB) CtxLogger(ctx *gin.Context) *zap.Logger {
	l := c.logger.With(zap.String("request_id", engine.GetRequestId(ctx)))
	return l
}

// Fully implemented Interface
type impl struct {
	engine  *gin.Engine                                           // gin Engine
	wrapper func(fn func(ctx *gin.Context) error) gin.HandlerFunc // gin handler wrapper function
	mu      sync.RWMutex                                          // kv mutex
	kv      map[string]interface{}                                // simple kv implemented by map
	dep     *dependency.Dependency                                // service dependencies
	manager configmanager.Manager                                 // config manager instance
}

func (i *impl) DB() *gorm.DB {
	return i.dep.Mysql()
}

func (i *impl) DBRead() *gorm.DB {
	return i.dep.Mysql()
}

func (i *impl) Tx() DBConn {
	t := i.dep.Mysql().Begin()
	return &conn{db: t, logger: i.dep.Logger()}
}

func (i *impl) CacheDB(tx bool) CacheDBConn {
	db := i.dep.Mysql()

	if tx {
		db = i.dep.Mysql().Begin()
		return &cacheDB{db: db, dbRead: nil, redis: i.dep.Redis(), logger: i.dep.Logger()}
	}
	return &cacheDB{db: db, dbRead: nil, redis: i.dep.Redis(), logger: i.dep.Logger()}
}

func (i *impl) Redis() *redis.Client {
	return i.dep.Redis()
}

func (i *impl) Logger() *zap.Logger {
	return i.dep.Logger()
}

func (i *impl) CtxLogger(ctx *gin.Context) *zap.Logger {
	l := i.dep.Logger().With(zap.String("request_id", engine.GetRequestId(ctx)))
	return l
}

func (i *impl) Dep() *dependency.Dependency {
	return i.dep
}

func (i *impl) ConfigManager() configmanager.Manager {
	return i.manager
}

func (i *impl) Wrapper(fn func(ctx *gin.Context) error) gin.HandlerFunc {
	return i.wrapper(fn)
}

func (i *impl) Engine() *gin.Engine {
	return i.engine
}

func (i *impl) Run() error {
	if i.dep.Configs.Service == nil {
		i.Logger().Info("service running")
		return i.Engine().Run(":8080")
	} else {
		i.Logger().Info("service running",
			zap.String("name", i.dep.Configs.Service.Name),
			zap.String("address", i.dep.Configs.Service.Address),
		)
		return i.Engine().Run(i.dep.Configs.Service.Address)
	}
}

func (i *impl) KVSet(k string, val interface{}) {
	i.mu.Lock()
	defer i.mu.Unlock()
	i.kv[k] = val
}

func (i *impl) KVGet(k string) (val interface{}, ok bool) {
	i.mu.RLock()
	defer i.mu.RUnlock()
	val, ok = i.kv[k]
	return
}

//func (i *impl) WithLogger(l *zap.Logger) *impl {
//	i.logger = l
//	return i
//}
//
//func (i *impl) WithDB(db *gorm.DB) *impl {
//	i.db = db
//	return i
//}
//
//func (i *impl) WithDBRead(db *gorm.DB) *impl {
//	i.dbRead = db
//	return i
//}
//
//func (i *impl) WithRedis(r *redis.Client) *impl {
//	i.redis = r
//	return i
//}
//
//func (i *impl) WithCustomDBCallbacks() *impl {
//	if i.db != nil {
//		model.SetCallbacks(i.db)
//	}
//	if i.dbRead != nil {
//		model.SetCallbacks(i.dbRead)
//	}
//	return i
//}

func (i *impl) WithEngine() *impl {
	enableAccessLog := false
	if i.dep.Configs.Log != nil {
		enableAccessLog = i.dep.Configs.Log.EnableAccessLog
	}
	i.engine = engine.NewGinEngine(i.dep.Logger(), enableAccessLog)
	i.wrapper = engine.NewAPIWrapper(i.dep.Logger())
	return i
}
