package core

import (
	"fmt"
	"net/url"
)

type ServiceConfig struct {
	Name      string
	SecretKey string `yaml:"secret_key" json:"secret_key"`
	Address   string
	Env       string
}

type DBConfig struct {
	Username string
	Password string
	Database string

	// optional
	Protocol  string                                      // default: tcp
	Address   string                                      // default: host:port
	Host      string                                      // default: localhost
	Port      int                                         // default: 3306
	Location  string                                      // default: UTC
	Charset   string                                      // default: utf8mb4
	ParseTime *bool `yaml:"parse_time" json:"parse_time"` // default: true
}

// DSN: username:password@protocol(address)/database?param=value
func (c *DBConfig) DSN() string {
	loc := url.QueryEscape(c.Location)
	parseTime := "true"
	if !*c.ParseTime {
		parseTime = "false"
	}
	dsn := fmt.Sprintf(
		"%s:%s@%s(%s)/%s?loc=%s&parseTime=%s&charset=%s",
		c.Username, c.Password, c.Protocol, c.Address, c.Database, loc, parseTime, c.Charset,
	)
	return dsn
}

// Anonymity returns connection info with password replaced by ***
func (c *DBConfig) Anonymity() string {
	dsn := fmt.Sprintf(
		"%s:***@%s(%s)/%s?loc=%s&parseTime=%v&charset=%s",
		c.Username, c.Protocol, c.Address, c.Database, c.Location, *c.ParseTime, c.Charset,
	)
	return dsn
}

// Complete auto completes config values
func (c *DBConfig) Complete() {
	if c.Protocol == "" {
		c.Protocol = "tcp"
	}
	if c.Address == "" {
		if c.Host == "" {
			c.Host = "localhost"
		}
		if c.Port == 0 {
			c.Port = 3306
		}
		c.Address = fmt.Sprintf("%v:%d", c.Host, c.Port)
	}
	if c.Location == "" {
		c.Location = "UTC"
	}
	if c.Charset == "" {
		c.Charset = "utf8mb4"
	}
	if c.ParseTime == nil {
		parse := true
		c.ParseTime = &parse
	}
}

type RedisConfig struct {
	Host     string // default: localhost
	Port     int    // default: 6379
	Database int    // default: 0
	Password string // default: ''
}

// DSN: host:port
func (c *RedisConfig) DSN() string {
	return fmt.Sprintf("%v:%v", c.Host, c.Port)
}

// Anonymity returns connection info as redis://***@host:port/database
func (c *RedisConfig) Anonymity() string {
	return fmt.Sprintf(
		"redis://***@%s:%v/%v",
		c.Host, c.Port, c.Database,
	)
}

func (c *RedisConfig) Complete() {
	if c.Host == "" {
		c.Host = "localhost"
	}
	if c.Port == 0 {
		c.Port = 6379
	}
}

type LogConfig struct {
	Level            string
	EnableAccessLog  bool `yaml:"enable_access_log" json:"enable_access_log"`     // default: false
	EnableCaller     bool `yaml:"enable_caller" json:"enable_caller"`             // default: false
	EnableStacktrace bool `yaml:"enable_stacktrace" json:"enable_stacktrace"`     // default: false
	EnableDBDebugLog bool `yaml:"enable_db_debug_log" json:"enable_db_debug_log"` // default: false
}

type BaseConfig struct {
	Service ServiceConfig `yaml:"service" json:"service"`
	Log     LogConfig     `yaml:"log" json:"log"`
	// Data source configs
	DBWrite *DBConfig    `yaml:"db" json:"db"`
	DBRead  *DBConfig    `yaml:"db_read" json:"db_read"`
	Redis   *RedisConfig `yaml:"redis" json:"redis"`
}
