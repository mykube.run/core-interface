### Usage

```go
var co core.Interface

func Setup(addr string) {
	// read base config
	var baseConf core.BaseConfig
	s := consul.NewSource(
		consul.WithAddress(addr),
		source.WithEncoder(yaml.NewEncoder()),
		consul.StripPrefix(true),
	)
	conf := config.NewConfig(config.WithSource(s))
	err := conf.Get("sandbox", "base").Scan(&baseConf)
	if err != nil {
		co.Logger().Fatal("load base config error", zap.Error(err))
	}

	// setup core interface
	co = core.New().WithBaseConfig(&baseConf).WithDB().WithCustomDBCallbacks().WithEngine()

	// migrate db
	err = co.DB().AutoMigrate(...).Error
	if err != nil {
		co.Logger().Fatal("migrate database error", zap.Error(err))
	}

	// setup api endpoints
	e := co.Engine()
	e.GET("api", co.Wrapper(...))
}

func Run() error {
	return co.Run()
}
	
// Place main func into cmd folder
func main() {
	var consul = flag.String("consul", "0.0.0.0:8500", "consul address")
	flag.Parse()
	Setup(*consul)
	log.Fatal(Run())
}
```