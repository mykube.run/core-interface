module gitlab.com/mykube.run/core-interface

go 1.13

require (
	github.com/aliyun/aliyun-oss-go-sdk v2.1.4+incompatible
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/jinzhu/gorm v1.9.12
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-micro/v2 v2.2.0
	github.com/micro/go-plugins v2.0.1+incompatible // indirect
	github.com/micro/go-plugins/config/source/consul v0.0.0-20200119172437-4fe21aa238fd
	github.com/micro/micro/v2 v2.2.0 // indirect
	github.com/satori/uuid v1.2.0
	go.mongodb.org/mongo-driver v1.4.0 // indirect
	go.uber.org/zap v1.14.0
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/yaml.v2 v2.2.7
)
