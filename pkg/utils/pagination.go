package utils

import (
	"github.com/jinzhu/gorm"
	"strings"
)

type Pagination struct {
	Page     int    `form:"page" json:"page" binding:"min=0"`
	PageSize int    `form:"page_size" json:"page_size" binding:"min=0"`
	OrderBy  string `form:"order_by" json:"order_by"` // Use Order() to handle descend ordering
	Export   bool   `form:"export" json:"export"`     // No pagination while exporting

	Count int `json:"count"` // Only returned in response
}

// Usage:
//
// func List(db *gorm.DB, p *Pagination) (out []Result, err error) {
//		query := db.Model(&Result{}).Where(...)
//		err = p.Paginate(query).Find(&out).Order(p.Order()).Error
//		return
// }
//
// func (c *gin.Context) {
// 		p = NewPaginator(page, pageSize).WithExport(false).WithOrderBy("updated_at DESC")
//		out, err := List(db, p)
//		c.JSON(200, p.Data())
// }
func NewPaginator(page, size int) *Pagination {
	return &Pagination{Page: page, PageSize: size}
}

func (p *Pagination) WithExport(export bool) *Pagination {
	p.Export = export
	return p
}

func (p *Pagination) WithOrderBy(k string) *Pagination {
	p.OrderBy = k
	return p
}

func (p *Pagination) Paginate(db *gorm.DB) (dbr *gorm.DB) {
	var count int
	err := db.Count(&count).Error
	if err != nil {
		return db
	}

	p.setup(count)
	dbr = db.Limit(p.PageSize).Offset(p.Offset())
	return
}

func (p *Pagination) Order() string {
	order := p.OrderBy
	// "-field" -> "field DESC"
	if p.OrderBy != "" && strings.HasPrefix(p.OrderBy, "-") {
		order = strings.Replace(p.OrderBy, "-", "", 1) + " DESC"
	}
	return order
}

func (p *Pagination) Offset() int {
	return p.PageSize * (p.Page - 1)
}

func (p *Pagination) Data(data interface{}) map[string]interface{} {
	out := map[string]interface{}{
		"data":       data,
		"pagination": *p,
	}
	return out
}

func (p *Pagination) setup(count int) {
	p.Count = count

	if p.Export {
		p.Page = 1
		p.PageSize = count
	} else {
		if p.PageSize <= 0 {
			p.PageSize = 20
		}
		if p.Page <= 0 {
			p.Page = 1
		}
		totalPages := p.Count/p.PageSize + 1
		if p.Page > totalPages {
			p.Page = totalPages
		}
	}
}
