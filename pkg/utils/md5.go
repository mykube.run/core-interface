package utils

import (
	"crypto/md5"
	"encoding/hex"
)

func MD5(in []byte) (out []byte) {
	h := md5.New()
	h.Write(in)
	hash := h.Sum(nil)
	return hash
}

func MD5String(in []byte) (out string) {
	h := md5.New()
	h.Write(in)
	hash := hex.EncodeToString(h.Sum(nil))
	return hash
}
