package utils

import (
	"strings"
)

func Str2Bool(in string) (out bool) {
	in = strings.ToUpper(in)
	if in == "TRUE" || in == "T" || in == "YES" || in == "Y" || in == "1" {
		return true
	}
	return false
}
