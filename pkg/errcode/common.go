package errcode

import "fmt"

// error struct
type ErrorCode struct {
	Code int    `json:"core"`
	Msg  string `json:"msg"`
}

func (e ErrorCode) Error() string {
	return fmt.Sprintf("error: %v", e.Code)
}

var (
	// Common: 10xx
	CodeInvalidParams = 1001
	CodeNotFound      = 1002

	Unauthorized  = ErrorCode{1003, "user is not authorized"}
	Forbidden     = ErrorCode{1004, "user has no permission to manipulate requested resource"}
	CodeForbidden = 1004
)
