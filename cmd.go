package main

import (
	"gitlab.com/mykube.run/core-interface/configmanager"
	"gitlab.com/mykube.run/core-interface/core"
	"log"
	"os"
	"time"
)

func main() {
	var err error
	// setup bootstrap envs
	bs := configmanager.DefaultBootstrapEnvs()
	if err = os.Setenv(bs.SourceType, configmanager.SourceTypeConsul); err != nil {
		log.Fatal(err)
	}
	if err = os.Setenv(bs.ServerAddr, "0.0.0.0:8500"); err != nil {
		log.Fatal(err)
	}
	if err = os.Setenv(bs.Group, "testing"); err != nil {
		log.Fatal(err)
	}

	manager, err := configmanager.NewDefaultConfigManager(bs, "/micro/config/core-interface")
	if err != nil {
		log.Fatal(err)
	}
	ins, err := core.New(manager)
	if err != nil {
		log.Fatal(err)
	}
	if ins == nil {
		log.Fatal("instance is nil")
	}

	if core.Instance() == nil {
		log.Fatal("instance() is nil")
	}

	time.Sleep(time.Second * 1000)
}
