package engine

import (
	"context"
	"github.com/gin-gonic/gin"
)

type MicroHandler func(ctx context.Context, req interface{}, resp interface{}) error

func MicroHandlerWrapper (c *gin.Context, h MicroHandler) gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Method == "POST" || c.Request.Method == "PUT" {

		}
	}
}