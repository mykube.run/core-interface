package engine

import (
	"github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/mykube.run/core-interface/pkg/errcode"
	"go.uber.org/zap"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"reflect"
	"runtime"
	"time"
)

const (
	KeyRequestId    = "request_id"
	HeaderRequestId = "X-Core-I-Request-ID"
)

func NewGinEngine(lg *zap.Logger, enableAccessLog bool) (e *gin.Engine) {
	// 1. Setup gin debug logger
	gin.DisableConsoleColor()
	gin.DebugPrintRouteFunc = func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
		lg.Info("debug log", zap.String("method", httpMethod),
			zap.String("path", absolutePath),
			zap.String("handler", handlerName),
		)
	}

	// 2. Create engine
	gin.SetMode(gin.ReleaseMode)
	e = gin.New()
	e.Use(RequestIdMiddleware())

	// 3. Setup access log
	if enableAccessLog {
		lg.Info("enabled access log")
		e.Use(AccessLoggerMiddleware(lg))
	}
	e.Use(gin.Recovery())

	// 4. Setup health check handler
	e.GET("/healthz", Healthz)
	return e
}

func NewAPIWrapper(lg *zap.Logger) func(fn func(ctx *gin.Context) error) gin.HandlerFunc {
	var fn = func(fn func(ctx *gin.Context) error) gin.HandlerFunc {
		return func(ctx *gin.Context) {
			err := fn(ctx)

			if err == nil {
				ctx.Header(HeaderRequestId, GetRequestId(ctx))
				return
			}

			// gin validation errors
			if ve, ok := err.(validator.ValidationErrors); ok {
				fields := make([]string, 0)
				for _, v := range ve {
					fields = append(fields, ToSnakeCase(v.Field()))
				}
				ctx.JSON(http.StatusOK, gin.H{
					"msg":    "invalid request parameters",
					"code":   errcode.CodeInvalidParams,
					"fields": fields,
				})

				// gorm not found error
			} else if gorm.IsRecordNotFoundError(err) {
				ctx.JSON(http.StatusOK, gin.H{
					"msg":  "resource not found",
					"code": errcode.CodeNotFound,
				})

				// service defined ErrorCode
			} else if ec, ok := err.(errcode.ErrorCode); ok {
				ctx.JSON(http.StatusOK, gin.H{
					"msg":  ec.Msg,
					"code": ec.Code,
				})

				// server error
			} else {
				eid := SimpleRandomString()
				handler := runtime.FuncForPC(reflect.ValueOf(fn).Pointer()).Name()
				lg.Error("internal server error", zap.Error(err), zap.String("error_id", eid),
					zap.String("method", ctx.Request.Method),
					zap.String("path", ctx.Request.URL.Path),
					zap.String("handler", handler),
				)

				ctx.JSON(http.StatusInternalServerError, gin.H{"msg": "internal server error", "eid": eid})
			}
		}
	}

	return fn
}

func AccessLoggerMiddleware(lg *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 1. Start timer
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		// 2. Process request
		c.Next()

		// 3. Populate params with timer and other information
		param := gin.LogFormatterParams{
			Request: c.Request,
			Keys:    c.Keys,
		}
		param.TimeStamp = time.Now()
		param.Latency = param.TimeStamp.Sub(start)

		param.ClientIP = c.ClientIP()
		param.Method = c.Request.Method
		param.StatusCode = c.Writer.Status()
		param.ErrorMessage = c.Errors.ByType(gin.ErrorTypePrivate).String()

		param.BodySize = c.Writer.Size()

		if raw != "" {
			path = path + "?" + raw
		}
		param.Path = path

		// 4. Start logging
		lg.Info("access log",
			zap.String("request_id", GetRequestId(c)),
			zap.Int("status_code", param.StatusCode),
			zap.Int64("duration", param.Latency.Nanoseconds()/1e6), // ms
			zap.String("ip", param.ClientIP),
			zap.String("method", param.Method),
			zap.String("path", param.Path),
			zap.String("err_message", param.ErrorMessage),
			zap.Int("response_size", param.BodySize), // bytes
		)
	}
}

func RequestIdMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(KeyRequestId, SimpleRandomString())
	}
}

func NewJwtAuthMiddleware(secret string, timeout time.Duration, getUserFunc func(uint) (interface{}, error)) (m *jwt.GinJWTMiddleware, err error) {
	var identityHandler = func(c *gin.Context) interface{} {
		claims := jwt.ExtractClaims(c)
		idVal := claims["sub"].(float64)
		id := uint(idVal)
		u, err := getUserFunc(id)
		if err != nil {
			return nil
		}
		// We store User here
		return u
	}

	var authorizator = func(data interface{}, c *gin.Context) bool {
		return data != nil
	}

	var unauthorizedHandler = func(c *gin.Context, code int, message string) {
		c.JSON(200, errcode.Unauthorized)
	}

	m = &jwt.GinJWTMiddleware{
		Realm:            "auth",
		SigningAlgorithm: "HS256",
		Key:              []byte(secret),
		Timeout:          timeout,
		IdentityHandler:  identityHandler,
		Authorizator:     authorizator,
		IdentityKey:      "user",
		TokenLookup:      "header: Authorization",
		TimeFunc:         time.Now,
		Unauthorized:     unauthorizedHandler,
	}

	m, err = jwt.New(m)
	return
}
