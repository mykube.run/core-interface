package engine

import (
	"github.com/gin-gonic/gin"
	"github.com/satori/uuid"
	"regexp"
	"strings"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")

var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func ToSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

func SimpleRandomString() string {
	raw := uuid.NewV4().String()
	spl := strings.Split(raw, "-")
	return spl[4]
}

func GetRequestId(c *gin.Context) string {
	var reqId string
	if val, ok := c.Get(KeyRequestId); ok {
		reqId = val.(string)
	} else {
		reqId = ""
	}
	return reqId
}

func Healthz(c *gin.Context) {
	c.JSON(200, gin.H{"msg": "ok"})
}
