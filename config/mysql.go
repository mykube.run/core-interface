package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/mykube.run/core-interface/model"
	"gitlab.com/mykube.run/core-interface/pkg/utils"
	"gopkg.in/yaml.v2"
	"net/url"
)

type MysqlConfig struct {
	// required
	Username string
	Password string
	Database string

	// optional
	Protocol              string // default: tcp
	Address               string // default: host:port
	Host                  string // default: localhost
	Port                  int    // default: 3306
	Location              string // default: UTC
	Charset               string // default: utf8mb4
	ParseTime             bool   `yaml:"parse_time" json:"parse_time"`                           // default: false
	UseTimestampCallbacks bool   `yaml:"use_timestamp_callbacks" json:"use_timestamp_callbacks"` // default false
	DebugLog              bool   `yaml:"debug_log" json:"debug_log"`                             // default: false

	// internal
	hash []byte
}

func (c *MysqlConfig) Validate() error {
	if c.Username == "" || c.Password == "" || c.Database == "" {
		return fmt.Errorf("invalid mysql config, username, password or database should not be empty")
	}

	if c.Protocol == "" {
		c.Protocol = "tcp"
	}
	if c.Address == "" {
		if c.Host == "" {
			c.Host = "localhost"
		}
		if c.Port == 0 {
			c.Port = 3306
		}
		c.Address = fmt.Sprintf("%v:%d", c.Host, c.Port)
	}
	if c.Location == "" {
		c.Location = "UTC"
	}
	if c.Charset == "" {
		c.Charset = "utf8mb4"
	}

	c.hash = c.Hash()

	return nil
}

func (c *MysqlConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *MysqlConfig) Marshal(enc Encoding) (data []byte, err error) {
	return
}

func (c *MysqlConfig) Anonymized() string {
	dsn := fmt.Sprintf(
		"%s:***@%s(%s)/%s?loc=%s&parseTime=%v&charset=%s",
		c.Username, c.Protocol, c.Address, c.Database, c.Location, c.ParseTime, c.Charset,
	)
	return dsn
}

func (c *MysqlConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *MysqlConfig) Copy() Config {
	cp := MysqlConfig{
		Username:              c.Username,
		Password:              c.Password,
		Database:              c.Database,
		Protocol:              c.Protocol,
		Address:               c.Address,
		Host:                  c.Host,
		Port:                  c.Port,
		Location:              c.Location,
		Charset:               c.Charset,
		ParseTime:             c.ParseTime,
		UseTimestampCallbacks: c.UseTimestampCallbacks,
		DebugLog:              c.DebugLog,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *MysqlConfig) Hash() []byte {
	return utils.MD5([]byte(c.abstract()))
}

// DSN: username:password@protocol(address)/database?param=value
func (c *MysqlConfig) DSN() string {
	loc := url.QueryEscape(c.Location)
	parseTime := "true"
	if !c.ParseTime {
		parseTime = "false"
	}
	dsn := fmt.Sprintf(
		"%s:%s@%s(%s)/%s?loc=%s&parseTime=%s&charset=%s",
		c.Username, c.Password, c.Protocol, c.Address, c.Database, loc, parseTime, c.Charset,
	)
	return dsn
}

func (c *MysqlConfig) abstract() string {
	abstract := fmt.Sprintf("%s:%v:%v", c.DSN(), c.UseTimestampCallbacks, c.DebugLog)
	return abstract
}

func (c *MysqlConfig) Connect() (db *gorm.DB, err error) {
	db, err = gorm.Open("mysql", c.DSN())
	if err != nil {
		return
	}

	if c.UseTimestampCallbacks {
		model.SetTimestampCallbacks(db)
	}
	if c.DebugLog {
		db = db.Debug()
	}
	return
}
