package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/mykube.run/core-interface/pkg/utils"
	"gopkg.in/yaml.v2"
)

type ServiceConfig struct {
	// required
	Name      string
	SecretKey string `yaml:"secret_key" json:"secret_key"`
	Address   string
	Env       string

	// internal
	hash []byte
}

func (c *ServiceConfig) Validate() error {
	if c.Name == "" {
		return fmt.Errorf("invalid service config, name should not be empty")
	}
	if c.SecretKey == "" {
		return fmt.Errorf("invalid service config, secret key should not be empty")
	}
	if c.Address == "" {
		return fmt.Errorf("invalid service config, address should not be empty")
	}
	if c.Env == "" {
		return fmt.Errorf("invalid service config, env should not be empty")
	}
	c.hash = c.Hash()
	return nil
}

func (c *ServiceConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *ServiceConfig) Marshal(enc Encoding) ([]byte, error) {
	return nil, nil
}

func (c *ServiceConfig) Anonymized() string {
	abstract := fmt.Sprintf("%v:%v:%v:***", c.Name, c.Address, c.Env)
	return abstract
}

func (c *ServiceConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *ServiceConfig) Copy() Config {
	cp := ServiceConfig{
		Name:      c.Name,
		SecretKey: c.SecretKey,
		Address:   c.Address,
		Env:       c.Env,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *ServiceConfig) Hash() []byte {
	return utils.MD5([]byte(c.abstract()))
}

func (c *ServiceConfig) abstract() string {
	abstract := fmt.Sprintf("%v:%v:%v:%v", c.Name, c.Address, c.Env, c.SecretKey)
	return abstract
}
