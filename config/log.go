package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/mykube.run/core-interface/pkg/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/yaml.v2"
	"strings"
	"time"
)

var (
	levels = []string{"DEBUG", "INFO", "WARN", "ERROR", "FATAL"}
)

type LogConfig struct {
	// required
	Level string

	// optional
	EnableAccessLog  bool `yaml:"enable_access_log" json:"enable_access_log"` // default: false
	EnableCaller     bool `yaml:"enable_caller" json:"enable_caller"`         // default: false
	EnableStacktrace bool `yaml:"enable_stacktrace" json:"enable_stacktrace"` // default: false

	// internal
	hash []byte
}

func (c *LogConfig) Validate() error {
	// check level
	var valid bool
	c.Level = strings.ToUpper(c.Level)
	for _, v := range levels {
		if c.Level == v {
			valid = true
			break
		}
	}

	if !valid {
		return fmt.Errorf("invalid log level: %v", c.Level)
	}

	c.hash = c.Hash()
	return nil
}

func (c *LogConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *LogConfig) Marshal(enc Encoding) ([]byte, error) {
	return nil, nil
}

func (c *LogConfig) Anonymized() string {
	return c.abstract()
}

func (c *LogConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *LogConfig) Copy() Config {
	cp := LogConfig{
		Level:            c.Level,
		EnableAccessLog:  c.EnableAccessLog,
		EnableCaller:     c.EnableCaller,
		EnableStacktrace: c.EnableStacktrace,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *LogConfig) Hash() []byte {
	return utils.MD5([]byte(c.abstract()))
}

func (c *LogConfig) abstract() string {
	abstract := fmt.Sprintf("%v:%v:%v:%v", c.Level, c.EnableAccessLog, c.EnableCaller, c.EnableStacktrace)
	return abstract
}

func (c *LogConfig) SetupLogger() (l *zap.Logger, err error) {
	var lvl zapcore.Level
	err = lvl.Set(c.Level)
	if err != nil {
		return
	}

	fmt.Printf(`{"level":"info","ts":%v,"msg":"setup logger","log_level":"%s"}`, time.Now().Unix(), c.Level)
	fmt.Println()
	zc := zap.NewProductionConfig()
	zc.Level = zap.NewAtomicLevelAt(lvl)
	zc.DisableCaller = !c.EnableCaller
	zc.DisableStacktrace = !c.EnableStacktrace
	l, err = zc.Build()
	return
}
