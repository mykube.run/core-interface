package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/mykube.run/core-interface/pkg/utils"
	"gopkg.in/yaml.v2"
	"time"
)

type RedisConfig struct {
	// required
	Host     string // default: localhost
	Port     int    // default: 6379
	Database int    // default: 0

	// optional
	Password string // default: ''

	// internal
	hash []byte
}

func (c *RedisConfig) Validate() error {
	if c.Host == "" {
		c.Host = "localhost"
	}
	if c.Port == 0 {
		c.Port = 6379
	}

	c.hash = c.Hash()
	return nil
}

func (c *RedisConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *RedisConfig) Marshal(enc Encoding) (data []byte, err error) {
	return
}

func (c *RedisConfig) Anonymized() string {
	return fmt.Sprintf(
		"redis://***@%s:%v/%v",
		c.Host, c.Port, c.Database,
	)
}

func (c *RedisConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *RedisConfig) Copy() Config {
	cp := RedisConfig{
		Host:     c.Host,
		Port:     c.Port,
		Database: c.Database,
		Password: c.Password,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *RedisConfig) Hash() []byte {
	abstract := fmt.Sprintf("%v:%v:%v:%v", c.Host, c.Port, c.Database, c.Password)
	return utils.MD5([]byte(abstract))
}

func (c *RedisConfig) DSN() string {
	return fmt.Sprintf("%v:%v", c.Host, c.Port)
}

func (c *RedisConfig) Connect() (r *redis.Client, err error) {
	opt := &redis.Options{
		Addr:        c.DSN(),
		DB:          int(c.Database),
		Password:    c.Password,
		DialTimeout: time.Second * 2,
	}
	r = redis.NewClient(opt)
	err = r.Ping().Err()
	return
}
