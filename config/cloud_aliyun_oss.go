package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"gopkg.in/yaml.v2"
)

type AliyunOSSConfig struct {
	// required
	AccessKeyId     string `yaml:"access_key_id" json:"access_key_id"`
	AccessKeySecret string `yaml:"access_key_secret" json:"access_key_secret"`
	Endpoint        string `yaml:"endpoint" json:"endpoint"`

	// internal
	hash []byte
}

func (c *AliyunOSSConfig) Validate() error {
	if c.AccessKeyId == "" {
		return fmt.Errorf("invalid aliyun config: access key id is empty")
	}
	if c.AccessKeySecret == "" {
		return fmt.Errorf("invalid aliyun config: access key secret is empty")
	}
	if c.Endpoint == "" {
		return fmt.Errorf("invalid aliyun config: endpoint is empty")
	}
	c.hash = c.Hash()
	return nil
}

func (c *AliyunOSSConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *AliyunOSSConfig) Marshal(enc Encoding) ([]byte, error) {
	return nil, nil
}

func (c *AliyunOSSConfig) Anonymized() string {
	return fmt.Sprintf("%s:***:%s", c.AccessKeyId, c.Endpoint)
}

func (c *AliyunOSSConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *AliyunOSSConfig) Copy() Config {
	cp := AliyunOSSConfig{
		AccessKeyId:     c.AccessKeyId,
		AccessKeySecret: c.AccessKeySecret,
		Endpoint:        c.Endpoint,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *AliyunOSSConfig) Hash() []byte {
	abstract := fmt.Sprintf("%s:%s:%s", c.AccessKeyId, c.AccessKeySecret, c.Endpoint)
	return []byte(abstract)
}

func (c *AliyunOSSConfig) Connect() (oc *oss.Client, err error) {
	oc, err = oss.New(c.Endpoint, c.AccessKeyId, c.AccessKeySecret)
	return
}
