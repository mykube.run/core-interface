package config

type Encoding string

const (
	EncJson Encoding = "json"
	EncYaml Encoding = "yaml"
)

type Config interface {
	// Validate validates configuration, fill in default values, generate & save hash itself
	Validate() error
	// Unmarshal unmarshal from given data and encoding
	Unmarshal(enc Encoding, data []byte) error
	// Marshal marshal configuration to bytes using specified encoding
	Marshal(enc Encoding) ([]byte, error)
	// Anonymized returns configuration with hidden credentials
	Anonymized() string
	// Updated checks whether configuration has been changed since last time
	Updated(hash []byte) bool
	// Copy makes an copy of current config
	Copy() Config
	// Hash returns hash as string
	Hash() []byte
}
