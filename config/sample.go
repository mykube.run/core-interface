package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/mykube.run/core-interface/pkg/utils"
	"gopkg.in/yaml.v2"
)

type SampleConfig struct {
	// required
	Required string

	// optional
	IntVal    int    `json:"int_val" yaml:"int_val"`
	BoolVal   bool   `json:"bool_val" yaml:"bool_val"`
	StringVal string `json:"string_val" yaml:"string_val"`

	// internal
	hash []byte
}

func (c *SampleConfig) Validate() error {
	if c.Required == "" {
		return fmt.Errorf("invalid sample config, key required should not be empty")
	}
	c.hash = c.Hash()
	return nil
}

func (c *SampleConfig) Unmarshal(enc Encoding, data []byte) error {
	if enc == EncYaml {
		return yaml.Unmarshal(data, c)
	} else {
		return json.Unmarshal(data, c)
	}
}

func (c *SampleConfig) Marshal(enc Encoding) ([]byte, error) {
	return nil, nil
}

func (c *SampleConfig) Anonymized() string {
	abstract := fmt.Sprintf("%v:%v:%v:***", c.IntVal, c.BoolVal, c.StringVal)
	return abstract
}

func (c *SampleConfig) Updated(hash []byte) bool {
	r := bytes.Compare(c.hash, hash)
	return r != 0
}

func (c *SampleConfig) Copy() Config {
	cp := SampleConfig{
		Required:  c.Required,
		IntVal:    c.IntVal,
		BoolVal:   c.BoolVal,
		StringVal: c.StringVal,
	}
	cp.hash = cp.Hash()
	return &cp
}

func (c *SampleConfig) Hash() []byte {
	return utils.MD5([]byte(c.abstract()))
}

func (c *SampleConfig) abstract() string {
	abstract := fmt.Sprintf("%v:%v:%v:%v", c.IntVal, c.BoolVal, c.StringVal, c.Required)
	return abstract
}
